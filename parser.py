from ruamel.yaml import YAML, YAMLError
from glob import glob
from os.path import join

yaml = YAML()
yaml.allow_duplicate_keys = True
yaml.preserve_quotes = True


# reads a yaml file
def read_file(file_path):
    with open(file_path) as file:
        local_yaml = file.read()
        try:
            multipart_local_data = yaml.load_all(local_yaml)
            return list(multipart_local_data)
        except YAMLError:
            print("incorrect yaml format", file_path)
            return None


# writes a yaml file
def write_file(file_path, data):
    with open(file_path, 'w') as file:
        file.write('---\n')
        yaml.dump_all(data, file)


# path filter
def path_filter(file_path):
    fil = ['beta', 'canary', 'ingress']
    if any(elem in file_path.split('/') for elem in fil):
        return False
    else:
        return True


# get yaml files
def get_yaml_files(dir_path):
    files = []
    for ext in ('*.yml', '*.yaml'):
        files.extend(glob(join(dir_path + "**/", ext), recursive=True))
    return list(filter(path_filter, files))


# create a production map
def create_production_map(dir_path):
    dic = {}
    limit = {}
    count = 0
    files = get_yaml_files(dir_path)
    for file in files:
        data = read_file(file)
        if data is not None:
            for doc in data:
                if 'kind' in doc:
                    if doc['kind'] == 'Deployment':
                        count += 1
                        dic[doc['metadata']['name']] = file
                        container = doc['spec']['template']['spec']['containers'][0]
                        if 'resources' in container:
                            limit[doc['metadata']['name']] = container['resources']
    print("Production Map created of ", count, " files")
    return dic, limit


# create a qa map
def create_qa_map(dir_path):
    dic = {}
    count = 0
    files = get_yaml_files(dir_path)
    for file in files:
        data = read_file(file)
        if data is not None:
            for doc in data:
                if 'kind' in doc:
                    if doc['kind'] == 'Deployment':
                        count += 1
                        dic[doc['metadata']['name']] = file
    return dic


# iterate qa and add limits to specific place
def add_limits_to_qa():
    qa_map = create_qa_map("/home/karangoel/Documents/Cleartrip/devops/qa/")
    prod_map, prod_limits = create_production_map("/home/karangoel/Documents/Cleartrip/devops/gke/prod/")
    for key in prod_map:
        if key in qa_map:
            file = qa_map[key]
            data = read_file(file)
            if data is not None:
                for doc in data:
                    if 'kind' in doc:
                        if doc['kind'] == 'Deployment':
                            if 'spec' in doc:
                                if 'replicas' in doc['spec']:
                                    doc['spec']['replicas'] = 1
                            if 'spec' not in doc:
                                doc['spec'] = {}
                            if 'template' not in doc['spec']:
                                doc['spec']['template'] = {}
                            if 'spec' not in doc['spec']['template']:
                                doc['spec']['template']['spec'] = {}
                            if 'containers' not in doc['spec']['template']['spec']:
                                doc['spec']['template']['spec']['containers'] = [{}]
                            if 'resources' not in doc['spec']['template']['spec']['containers'][0]:
                                doc['spec']['template']['spec']['containers'][0]['resources'] = {}
                            doc['spec']['template']['spec']['containers'][0]['resources'] = prod_limits[key]
                            write_file(qa_map[key], data)


add_limits_to_qa()
